# Book

本页面为登录后首页，用于查看不同Broker组别的PNL偏量

## Floating PNL

- 上方方格中简要显示不同组当前的Floating PNL
- 可通过更改Book按钮更改所选组别，以显示不同组别的详情

![SummaryPNL](../.vuepress/public/image/book/summaryPNL.png)

## Position Keeper

- 页面中间方格中分别显示所选组别的Floating PNL（EOD）、Closed PNL以及Net PNL
  - Floating PNL（EOD）：该浮动盈亏额采用End of Day Price计算方式，单子使用昨日平仓价格计算浮动盈亏
  - Closed PNL：该组所有用户当日已关闭单子盈亏总和
  - Net PNL：前两项之和

![PositionKeeperBook](../.vuepress/public/image/book/positionKeeperBook.png)

- 下方表格中显示不同产品的持仓情况，可以通过进行勾选调整表格内显示内容，该表格还可以进行搜索筛选、下载Excel表格等操作
  - Symbol：产品名
  - Long：多单
  - Short：空单
  - Net：总持仓手数
  - Hedge Net：对冲总持仓
  - Hedge PNL：对冲PNL
  - Floating PNL（EOD）：采用End of Day Price计算的浮动盈亏额，单子使用昨日平仓价格计算浮动盈亏
  - Floating PNL（MT4）：采用MT4计算的浮动盈亏额，单子从开单日至今的浮动盈亏
  - Closed PNL：当日已关闭单子盈亏总和
  - Net PNL：Floating PNL（EOD）与Closed PNL的综合
  - Avg Buy Price：平均购买价格
  - Avg Sell Price：平均售出价格
  - Eod：鼠标放置于表中该项时会显示该产品不同后缀产品的End of Day Price

![detailsPNL](../.vuepress/public/image/book/detailsPNL.png)

- 点击表中不同产品所在行，弹出的列表中会显示该产品持仓用户详情，鼠标放置于账户Login上时会显示该账户统计数据
  - Login：用户账户账号
  - Vol Long：用户该产品多单
  - Vol Short：用户该产品空单
  - Vol Net：Vol Long以及Vol Short总和
  - Avg Buy：平均购买价格
  - Avg Sell：平均售出价格

![symbolDetails](../.vuepress/public/image/book/symbolDetails.png)

## Exposure Chart

![ExposureChart](../.vuepress/public/image/book/exposureChart.png)

