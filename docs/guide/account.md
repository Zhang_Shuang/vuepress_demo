# Account
本界面可以进行账户设定的更改，包括修改密码等。

## Settings

### Security

#### Change Password

在该界面可以进行密码修改。

 ![Change Password](../.vuepress/public/image/account/settings/security.png)

### Site Preference

