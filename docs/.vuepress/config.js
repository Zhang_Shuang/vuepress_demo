module.exports = {
    title: 'Position Keeper',//网站标题
    description: 'A Guide for PK',//网站描述
    //host: //默认"0.0.0.0"
    //port: //默认"8080"
    // head: [["link", { rel: "icon", href: `/logo.png` }]], //标签注入
    base: "/",
    dest: "./dist",
    themeConfig: {
        search: true, //允许搜索
        nav: [ //导航栏
            { text: "Home", link: "/" },
            { text: "About", link: "/about/" },
            { text: "Guide", link: "/guide/" },
        ],
        sidebar: {
            '/guide/': guideSidebarConfig('Guide')
        },
        lastUpdated: 'Last Updated'
    },
    markdown: {
        // options for markdown-it-anchor
        anchor: { permalink: false },
    }
}
function guideSidebarConfig(title) {
    return [
        {
            title,
            collapsable: false,//折叠
            children: [
                '',//入口
                'account'
            ]
        }
    ]
}
